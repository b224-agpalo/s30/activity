

db.fruits.aggregate(
    [
        {
            $match: {
                onSale: true
            }
        },
        {
            $count: "fruitsOnSale"
        }
    ]
);

db.fruits.aggregate(
    [
        {
            $match: {
                stock: { $gte: 20 }
            }
        },
        {
            $count: "enoughStocks"
        }
    ]
);

db.fruits.aggregate([
    {
        $match: {
            onSale: true
        }

    },
    { $group: { _id: "$supplier_id", total: { $avg: "$price" } } }


]);

db.fruits.aggregate([
    {
        $match: {
            onSale: true
        }

    },
    { $group: { _id: "$supplier_id", total: { $max: "$price" } } }


]);
db.fruits.aggregate([
    {
        $match: {
            onSale: true
        }

    },
    { $group: { _id: "$supplier_id", total: { $min: "$price" } } }


]);


